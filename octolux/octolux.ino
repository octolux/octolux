/** Includes **/
#include "octolux.h"
#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include <ESP8266mDNS.h>          //Allow custom URL
#include <DNSServer.h>            //Local DNS Server used for redirecting all requests to the configuration portal
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager WiFi Configuration Magic
#include <WiFiClient.h>
#include <ESP8266WebServer.h>     //Local WebServer used to serve the configuration portal
#include <Adafruit_NeoPixel.h>    //https://github.com/adafruit/Adafruit_NeoPixel
#include <elapsedMillis.h>        //https://github.com/pfeerick/elapsedMillis


/****Config****/
#define SIZE 15
#define NUMBER 4
#define COLOR_MAX 128

#define  LED_STRIP_PIN_1  D1 /* LED STRIP PIN - AKA GPIO5 on witty modules */
#define  LED_STRIP_PIN_2  D2 /* LED STRIP PIN - AKA GPIO5 on witty modules */
#define  LED_STRIP_PIN_3  D3 /* LED STRIP PIN - AKA GPIO5 on witty modules */
#define  LED_STRIP_PIN_8  D8 /* LED STRIP PIN - AKA GPIO5 on witty modules */


typedef enum behavior_e {
	NO_BEHAVIOR = 'd',
	BREATHING = 'b'
} behavior_t;

/** Tentacle structure used for manage tentacles independently **/
struct tentacle {
	bool states[SIZE]   = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};
	int r[SIZE]         = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, 
	    g[SIZE]         = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, 
	    b[SIZE]         = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, 
	    bright[SIZE]    = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	behavior_t behavior = NO_BEHAVIOR;
};


int division = 30;
int brightness = COLOR_MAX / division;
bool upOrDownBrightness = true;


elapsedMillis elapsedTime;


/* Set these to your desired credentials. */
const char *ssid = "Octolux";

ESP8266WebServer server(80);


/** Create a new tentacle **/
tentacle create_tentacle() {
	tentacle tmp;

	for (int i = 0; i < SIZE; i++) {
		tmp.r[i] = 9;
		tmp.g[i] = 9;
		tmp.b[i] = 9;
		tmp.bright[i] = 25;
	}

	return tmp;
}


/** Initialisation of arrays for further use **/
tentacle tentacles[NUMBER] = {
	create_tentacle(),
	create_tentacle(),
	create_tentacle(),
	create_tentacle()
};

Adafruit_NeoPixel pixels[NUMBER] = {
	Adafruit_NeoPixel(SIZE, LED_STRIP_PIN_8, NEO_GRB + NEO_KHZ800),
	Adafruit_NeoPixel(SIZE, LED_STRIP_PIN_1, NEO_GRB + NEO_KHZ800),
	Adafruit_NeoPixel(SIZE, LED_STRIP_PIN_2, NEO_GRB + NEO_KHZ800),
	Adafruit_NeoPixel(SIZE, LED_STRIP_PIN_3, NEO_GRB + NEO_KHZ800)
};


/*** HANDLERS ***/
/** GET / **/
void handleRoot() {
	server.send(200, "text/html", root_html);
}


/** GET /tentacle/color **/
void handle_tentacle_color() {
	if (!server.hasArg("num") || !server.hasArg("r") || !server.hasArg("g") || !server.hasArg("b")) {
		server.send(400, "text/html", "<h1>ERROR</h1>");
		return;  
	}
	
	set_one_color_to_a_tentacle(
		server.arg(0).toInt(),
		server.arg(1).toInt(),
		server.arg(2).toInt(),
		server.arg(3).toInt()
	);
	server.send(200, "text/html", "<h1>Tentacle color settled</h1>");
}


/** GET /tentacle/behavior **/
void handle_tentacle_behavior() {
	if (!server.hasArg("num") || !server.hasArg("mode")) {
		server.send(400, "text/html", "<h1>ERROR</h1>");
		return;
	}

	tentacle_set_behavior(server.arg(0).toInt(), (behavior_t)server.arg(1).charAt(0));
	server.send(200, "text/html", "<h1>Tentacle behavior settled</h1>");
}


/****Setups****/
/** Setup serial port **/
void seupt_serial() {
	delay(1000);
	Serial.begin(115200);
}


/** Setup Wifi **/
void setup_wifi() {
	WiFiManager wifiManager;
	
	// Reset of server configuration
	// wifiManager.resetSettings();

	// fetches ssid and pass from eeprom and tries to connect
	// if it does not connect it starts an access point with the specified name
	// and goes into a blocking loop awaiting configuration
	wifiManager.autoConnect(ssid);

	Serial.println("Configuring access point...");
	IPAddress myIP = WiFi.softAP(ssid);

	Serial.print("AP IP address: ");
	Serial.println(myIP);
	Serial.println(WiFi.localIP());
}


/** Setup access port and dns **/
void setup_MDNS() {
	// Add service to MDNS-SD to access the ESP with the URL http://<ssid>.local
	if (MDNS.begin(ssid)) {
		Serial.print("MDNS responder started as http://");
		Serial.print(ssid);
		Serial.println(".local");
	}
	MDNS.addService("http", "tcp", 8080);
}


/** Setup server **/
void setup_server() {  
	server.on("/", handleRoot);
	server.on("/tentacle/color", handle_tentacle_color);
	server.on("/tentacle/behavior", handle_tentacle_behavior);
	
	server.begin();
	Serial.println("HTTP server started");
}


void setup() {
	// put your setup code here, to run once:
	pinMode(BUILTIN_LED, OUTPUT);
	digitalWrite(BUILTIN_LED, LOW); // = ON (inverted logic)

	seupt_serial();
	setup_wifi();
	setup_MDNS();
	setup_server();

	for (int i = 0; i < NUMBER; i++) {
		pixels[i].begin();
	}

	Serial.println("Setup OK.");
}


/** Check a inputted color and correct it **/
int valid_color(int color) {
	return color % (COLOR_MAX + 1); 
}


void set_one_color_to_a_tentacle(int index, int red, int green, int blue) {
	red = valid_color(red);
	green = valid_color(green);
	blue = valid_color(blue);

	for (int i = 0; i < SIZE; i++) {
		tentacles[index].r[i] = red;
		tentacles[index].g[i] = green;
		tentacles[index].b[i] = blue;
		tentacles[index].states[i] = 1;
	}
}


void tentacle_set_behavior(int index, behavior_t mode) {
	tentacles[index].behavior = mode;
}


void tentacle_set_brightness() {
	brightness = upOrDownBrightness ? brightness + (COLOR_MAX / division) : brightness - (COLOR_MAX / division);
	if (brightness >= COLOR_MAX) {
		brightness = COLOR_MAX;
		upOrDownBrightness = false;
	}
	else if (brightness <= 0) {
		brightness = COLOR_MAX / division;
		upOrDownBrightness = true;
	}
}


void do_behavior(tentacle tentacle_active, int tentacle_index) {
	if (tentacle_active.behavior == BREATHING) {
		pixels[tentacle_index].setBrightness(brightness);
	}
}


/** Update the pixels of all tentacles **/
void non_blocking_led_control(int waitMs) {
	// non blocking delay:
	if (elapsedTime < waitMs) return;

	elapsedTime = 0;
	digitalWrite(BUILTIN_LED, !digitalRead(BUILTIN_LED));

	// "loop" on colors:
	for (int ti = 0; ti < NUMBER; ti++) {
		for (int i = 0; i < pixels[ti].numPixels(); i++) {          
			if (tentacles[ti].states[i]) {
				uint32_t color = pixels[ti].Color(
					tentacles[ti].r[i],
					tentacles[ti].g[i],
					tentacles[ti].b[i]
				);
				pixels[ti].setPixelColor(i, color);
			}
			else {
				pixels[ti].setPixelColor(i, 0);
			}
		}

		do_behavior(tentacles[ti], ti);

		pixels[ti].show();
	}

	tentacle_set_brightness();
}


void loop() {
	server.handleClient();
	non_blocking_led_control(100);
}
