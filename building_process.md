# Building Process

- [Building Process](#building-process)
    - [Hardware](#hardware)
    - [Electric Schema](#electric-schema)
    - [Adapt to your installation](#adapt-to-your-installation)


## Hardware

- [ESP8266 Arduino][arduino_hardware]
- [Raspberry Pi Transparent Case][raspberry_pi_case]
- WS2812B LED strip
- Electrical cables


## Electric Schema

![Electric Schema](static/electric_schema.png)


Note: the alimentation will be powered by the USB cable


## Adapt to your installation

To adapt to your installation you'll have to change these lines of code in [the Arduino source file](./octolux/octolux.ino)

```cpp
// adapt this number
#define NUMBER 4

// [...]

// add the pin that you use
#define LED_STRIP_PIN_1 D1

// [...]

// adapt the number of row
tentacle tentacles[NUMBER] = {
	create_tentacle(),
	create_tentacle(),
	create_tentacle(),
	create_tentacle()
};

// [...]

// adapt the number of row with your pins
Adafruit_NeoPixel pixels[NUMBER] = {
	Adafruit_NeoPixel(SIZE, LED_STRIP_PIN_8, NEO_GRB + NEO_KHZ800),
	Adafruit_NeoPixel(SIZE, LED_STRIP_PIN_1, NEO_GRB + NEO_KHZ800),
	Adafruit_NeoPixel(SIZE, LED_STRIP_PIN_2, NEO_GRB + NEO_KHZ800),
	Adafruit_NeoPixel(SIZE, LED_STRIP_PIN_3, NEO_GRB + NEO_KHZ800)
};
```


[arduino_hardware]: https://github.com/esp8266/Arduino#installing-with-boards-manager
[raspberry_pi_case]: http://www.priceminister.com/offer/buy/1908621415/10pcs-raspberry-pi-3-b-case-framboise-pi-512-rev3-rpi3-box-case-shell-raspberry-pi-3-b-acrylique-shell-dernieres-box.html?bbaid=2665775965&sort=0&xtatc=PUB-%5Bggp%5D-%5BInformatique%5D-%5BAccessoires%5D-%5B1908621415%5D-%5Bneuf%5D-%5BYOINS%5D&ptnrid=pt%7C89152206803%7Cc%7C53434268603%7C1908621415&t=180112&ptnrid=s24llCvmK_dc|pcrid|53434268603|pkw||pmt|&ja1=tsid:67590|cid:285670043|agid:14467128323|tid:pla-89152206803|crid:53434268603|nw:g|rnd:15280601699857779738|dvc:c|adp:4o19&gclid=CjwKEAjwkq7GBRDun9iu2JjyhmsSJADHCD_HYJ_TfXkhiG2UCp2HAsWgwtt_zdcaPynK-i0VDb9RIBoCy0Pw_wcB
