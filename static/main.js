const timer = (() => {
	const _timer = new Map();

	return ({name, callback, delay=50, callback_arg=[]}) => {
		const timer_id = _timer.get(name);
		
		if (timer_id) {
			clearTimeout(timer_id);
		}

		const callback_wrapper = () => {
			_timer.set(name, 0);
			callback(callback_arg);
		};

		_timer.set(name, setTimeout(callback_wrapper, delay));
	};
})();


function color_hex_to_rgb_url(color) {
	let url = '';
	url +=  'r=' + parseInt(color.substring(1,3), 16);
	url += '&g=' + parseInt(color.substring(3,5), 16);
	url += '&b=' + parseInt(color.substring(5,7), 16);
	return url;
}


function url_add_argument(url, url_argument) {
	return url + (url.includes('?') ? '&' : '?') + url_argument;
}


function submit({form, name, value}) {
	let url_argument;
	if (name === 'behavior') {
		url_argument = 'mode=' + value;
	}
	else {
		url_argument = color_hex_to_rgb_url(value);
	}

	const url = url_add_argument(form.action, url_argument);
	fetch(url)
		.then(response => response.text()
			.then(message => {
				if (!response.ok) {
					alert(message);
				}
				else {
					console.debug(message);
				}
			})
		)
		.catch(err => alert(err))
	;
}


function submit_on_input(form) {
	return ev => {
		const input = ev.target;
		timer({
			name    : form.action,
			callback: submit,
			callback_arg: {
				form : form,
				name : input.name,
				value: input.value,
			},
		});
	};
}


function main() {
	Array.from(document.querySelectorAll('.form'))
		.forEach(form => form.addEventListener('input', submit_on_input(form)))
	;
}


main();
