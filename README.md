![Octolux logo][octolux_logo]
# Octolux


This project show some colored light


- [Octolux](#octolux)
    - [Building process](#building-process)
    - [Software Dependencies](#software-dependencies)
    - [Install](#install)
        - [Clone project](#clone-project)
        - [Install dependencies](#install-dependencies)
        - [Setup environment](#setup-environment)
            - [Setup the board](#setup-the-board)
            - [Add external libraries](#add-external-libraries)
        - [Update the HTML](#update-the-html)
        - [Upload `octolux` project to your Arduino](#upload-octolux-project-to-your-arduino)
        - [Set the network connection](#set-the-network-connection)
        - [Controle the `octolux`](#controle-the-octolux)
    - [Known issues](#known-issues)
    - [Used content](#used-content)
    - [State of the art](#state-of-the-art)




## Building process

[See building process](building_process.md)



## Software Dependencies

[Arduino IDE][arduino_ide]

If you use windows / OSx you will probably need drivers: [Wemos Driver][wemos_driver]

[Git][git]

[NodeJS][nodejs]

[Yarn][yarn]



## Install

### Clone project

```sh
git clone https://gitlab.com/octolux/octolux/
```


### Install dependencies

```sh
cd octolux
yarn install
```


### Setup environment

1. Start Arduino IDE


#### Setup the board

1. Open the Preferences window
2. In the Additional Board Manager URLs field, enter: [ESP8266 Arduino Driver][arduino_driver]
3. Open `Tools` -> `Board` -> `Boards Manager...`
4. Type `esp` and the `esp8266` platform will appear, install it
5. Select your board: `Tools` -> `Board` -> `Wemos D1 R2 & mini` (or `nodeMCU` for some)


#### Add external libraries

1. Open `Sketch` -> `Include Library` -> `Manage Libraries...`
2. Install [`WiFiManager`][wifi_manager]
3. Install `Adafruit NeoPixel`
4. Install `elapsedMillis`


### Update the HTML

```sh
./minify.sh
```


### Upload `octolux` project to your Arduino

1. Open the octolux project
2. Open monitor: `Tools` -> `Serial Monitor`
3. Maybe you will have to select the port: `Tools` -> `Port`


### Set the network connection

1. Upload the program, you should now see the access point `octolux` on your computer !
2. Connect your computer to it
3. Open your webbrowser
4. Configure the network by the web interface (SSID + password)


### Controle the `octolux`

1. On your computer, connect to the same network that you've configured
2. You should see the IP address on the `Serial Monitor`
3. On your webbrowser connect to this IP address you see the webinterface
4. Change the behaviors & colors
5. Enjoy !


## Known issues

We've got a problem while we connecting the `ESP8266MOD` to the `WS2812B` using the pin `D4`, the first LED got another color than the others


## Used content

[Base image for our logo][base_image_logo]


## State of the art

[The Light Clock ![The Light Clock image][the_light_clock_image]][the_light_clock]

[Techno-Tiki RGB LED Torch ![Techno-Tiki RGB LED Torch image][techno_tiki_rgb_led_torch_image]][techno_tiki_rgb_led_torch]

[Samata's Necklace ![Samata's Necklace image][samata_s_necklace_image]][samata_s_necklace]



[octolux_logo]:     https://octolux.gitlab.io/octolux/logo.svg

[arduino_ide]:      https://www.arduino.cc/en/Main/Software
[wemos_driver]:     https://www.wemos.cc/downloads
[git]:              https://git-scm.com/downloads
[nodejs]:           https://nodejs.org/en/
[yarn]:             https://yarnpkg.com/lang/en/docs/install/
[arduino_driver]:   http://arduino.esp8266.com/versions/2.3.0/package_esp8266com_index.json
[wifi_manager]:     https://github.com/tzapu/WiFiManager#installing
[base_image_logo]:  http://www.how-to-draw-funny-cartoons.com/image-files/octopus-cartoon-001.jpg

[the_light_clock_image]: https://cdn.hackaday.io/images/7181301447157329052.jpg
[the_light_clock]:       https://hackaday.io/project/6925-the-light-clock

[techno_tiki_rgb_led_torch_image]: https://cdn-learn.adafruit.com/assets/assets/000/026/732/original/leds_20150725_0011.jpg?1437977184
[techno_tiki_rgb_led_torch]:       https://learn.adafruit.com/techno-tiki-rgb-led-torch/soldering?view=all

[samata_s_necklace_image]: https://cdn.hackaday.io/images/7626411431583723816.jpg
[samata_s_necklace]:       https://hackaday.io/project/5798-samatas-necklace
